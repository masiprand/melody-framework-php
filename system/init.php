<?php
require_once "Helper/functions.php";
require_once "app/config/config.php";
require_once "app/config/auto_load.php";
require_once "define.php";
require_once "Driver/Driverpdo.php";
require_once 'Helper/Input.php';

spl_autoload_register(function($class){
    require_once $class.'.php';
});
