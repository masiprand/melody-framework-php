<?php
class Database {
  public $driver;
  public function __construct()
  {
    //$this->driver = new Driverpdo;
  }
  
  public function select_from($table,$bintang="*")
  {
    $this->driver = new Driverpdo;
    $this->driver->query("SELECT ".$bintang." FROM ".$table);
    return $this->driver->resultSet();
  }
  //valuewhere isi dengan erray
  public function select_where($table,$valueWhere=[])
  {
    $this->driver = new Driverpdo;
    
    $prep = []; 
    foreach($valueWhere as $k => $v ) { 
      $prep[':'.$k] = $v;
      $bind = $v;
    }
    $aksi = implode(', ',array_keys($valueWhere));
    $value = implode(', ',array_keys($prep));

    $this->driver->query("SELECT * FROM ".$table." WHERE ".$aksi."=".$value);
    $this->driver->bind($aksi,$bind);
    return $this->driver->single();
  }
  //$insert isi dengan erray
  public function insert($table,$insert)
  {
    $this->driver = new Driverpdo;
    $prep = []; 
    foreach($insert as $k => $v ) { 
      $prep[':'.$k] = $v;
      $bind = $k;
    }
    
    $aksi = implode(', ',array_keys($insert));
    $value = implode(', ',array_keys($prep));
    $query = "INSERT INTO ".$table." (".$aksi.") VALUES (".$value.")";
    $this->driver->query($query);
    
    foreach($insert as $a => $b ) { 
      $pr[$a] = $b;
      $this->driver->bind($a,$b);
    }
    
    $this->driver->execute();
    
    
    
    
    return $this->driver->rowCount();
    
  }
  //$where dan $update isi array
  public function update_where($table,$where,$update)
  {
  
    $this->driver = new Driverpdo;
    $prep = []; 
    $w = [];
    foreach($update as $k => $v ) { 
      $prep[$k.'=:'.$k] = $v;
      
    }
    foreach($where as $kw => $vw ) { 
      $w[':'.$kw] = $vw;
      
    }
   
    $where1 = implode(', ',array_keys($where));
    $where2 = implode(', ',array_keys($w));
    $aksi = implode(', ',array_keys($update));
    $value = implode(', ',array_keys($prep));
     
    
    $query = "UPDATE ".$table." SET ".$value." WHERE ".$where1."=".$where2;
    $this->driver->query($query);
    
    foreach($update as $key => $vl ) { 
      $this->driver->bind($key,$vl);
    }
    
    foreach($where as $ky => $vll ) { 
      $this->driver->bind($ky,$vll);
    }
    
    $this->driver->execute();
    
    
    
    
    return $this->driver->rowCount();
  }
  
  public function delete_from($table)
  {
    $this->driver = new Driverpdo;
    $query = "DELETE FROM ".$table;
    $this->driver->query($query);
    $this->driver->execute();
    return $this->driver->rowCount();
  }
  
  public function delete_where($table,$delt)
  {
    $this->driver = new Driverpdo;
    
    $prep = []; 
    foreach($delt as $k => $v ) { 
      $prep[':'.$k] = $v;
    }
    
    $aksi = implode(', ',array_keys($delt));
    $value = implode(', ',array_keys($prep));

    $query = "DELETE FROM ".$table." WHERE ".$aksi."=".$value;
    $this->driver->query($query);
    foreach($delt as $a => $b){
      $this->driver->bind($a,$b);
    }
    $this->driver->execute();
    
    return $this->driver->rowCount();
  }
  
  public function select_from_where($table,$valueWhere=[],$start="*")
  {
    $this->driver = new Driverpdo;
    
    $prep = []; 
    foreach($valueWhere as $k => $v ) { 
      $prep[':'.$k] = $v;
      $bind = $v;
    }
    $aksi = implode(', ',array_keys($valueWhere));
    $value = implode(', ',array_keys($prep));

    $this->driver->query("SELECT ".$start." FROM ".$table." WHERE ".$aksi."=".$value);
    $this->driver->bind($aksi,$bind);
    return $this->driver->single();
  }
  
  
  
  
}

