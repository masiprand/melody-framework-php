<?php
class Validasi {
  private $_start=false;
  private $_error=[];
  private $_input;
  private $_db;
  
  public function __construct()
  {
  
    require_once 'system/library/Database.php';
    $this->_input = new Input;
    $this->_db = new Database;
  }
  public function setRule($items=[])
  {
    
      $post = $_POST;
    
    foreach($items as $item => $rules){
      foreach($rules as $rule => $rule_value){
        $value = trim($post[$item]);
        $alias = $rules["alias"];
        
        
        if($rule === "required" && empty($value)){
          $this->addError($item,$alias." is required");
        }elseif(!empty($value)){
          switch($rule){
            case 'min' :
              if(strlen($value) < $rule_value){
                $this->addError($item,$alias." must be a minimum of ".$rule_value." characters");
              }
            break;
            case 'max' :
              if(strlen($value) > $rule_value){
                $this->addError($item,$alias." must be a minimum of ".$rule_value." characters");
              }
            break;
            case 'matches' :
              if($value != $post[$rule_value]){
                $this->addError($item,$value[$rule_value]." must match ".$alias );
              }
            break;
            case 'is_unique' :
              $getDb = $this->_db->select_from_where($rule_value,[$item => $value],$item);
              if($getDb != false){
                $this->addError($item,$alias." ".$value." allready exists");
              }
            break;
            case 'valid_email' :
              if(!filter_var($value,FILTER_VALIDATE_EMAIL)){
                $this->addError($item,"This ".$alias." is a invalid email");
              }
            break;
            case 'numeric' :
              if(!is_numeric($value)){
                $this->addError($item,$alias." please numeric value");
              }
            break;
            
          }
        }
        
      }
      
    }
    
    if(empty($this->_error)){
      $this->_start = true;
    }
    
    return $this;
  
  }
  
  
  private function addError($item,$error)
  {
    
    $this->_error[$item] = $error;
   
  }
  
  public function valid_message($name,$awal="",$akir="")
  {
    if($this->_input->exist()){
      return $awal.$this->_error[$name].$akir;
    }
    
    
  }
  public function start()
  {
    
    return $this->_start;
  }
  
  
}

