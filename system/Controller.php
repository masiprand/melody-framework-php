<?php
class Controller {
  public $call;
  public $db;
  public $x;
  public $form_valid;
  public $input;
  public $autodb = AUTODB;
  public $autoFD = VALIDASI;
  
  
  public function __construct()
  {
    
    
    $this->input = new Input;
    
    
    
    if($this->autodb === true){
      $this->db = $this->load("Database","library");
    }
    
    if(VALIDASI === true){
      $this->form_valid = $this->load("Validasi","library");
   
    }
   
  }
  
  public function load($iscall,$namecall,$data=[])
  {
   
    $ar = [$iscall => $iscall];
    
    //view
    if(isset($ar["view"])){
      if(file_exists('app/views/'.$namecall.'.php')){
        require_once 'app/views/'.$namecall.'.php';
        
      }else{
        echo '<h2>Sory!! ,app/views/'.$namecall.'.php, Not exist</h2>';
        die();
      }
    }
    //model
    if(isset($ar["model"])){
      if(file_exists('app/models/'.$namecall.'.php')){
        require_once 'app/models/'.$namecall.'.php';
        return new $namecall;
      }else{
        echo '<h2>Sory!! ,app/models/'.$namecall.'.php, Not exist</h2>';
        die();
      }
    }
    //library
    if(isset($ar["library"])){
      if(file_exists('app/lib/'.$namecall.'.php')){
        require_once 'app/lib/'.$namecall.'.php';
        return new $namecall;
      }else{
        echo '<h2>Sory!! ,app/lib/'.$namecall.'.php, Not exist</h2>';
        die();
      }
    }
    
    
    //database
    if(isset($ar["Database"])){
      
      require_once 'system/'.$namecall.'/Database.php';
      $this->db = new Database;
      return new Database;
    }
    
    if(isset($ar["Validasi"])){
      
      require_once 'system/'.$namecall.'/Validasi.php';
      $this->form_valid = new Validasi;
      return new Validasi;
    }
    
    
   }
   
   
    
    
    
  
}
  
  
  
 

