<?php

define("BASE_URL",$config["BASE_URL"]);
define("CONTROLLER",$config["CONTROLLER"]);
define("DBHOST",$config["DBHOST"]);
define("DBUSER",$config["DBUSER"]);
define("DBPASS",$config["DBPASS"]);
define("DBNAME",$config["DBNAME"]);
define("DRIVER",$config["DRIVER"]);

define("AUTODB",$library["database"]);
define("VALIDASI",$library["form-validation"]);