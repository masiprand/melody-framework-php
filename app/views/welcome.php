<!DOCTYPE html>
<html>
<head>
  <!-- Required meta tags --> 
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 

  <title>Welcome</title>
  <style>
    body {
      background-color: rgba(0,0,0,0.6);
    }
    .container {
      margin-top:200px;
    }
    .header {
      background-color: white;
      border-radius:5px;
      padding:10px
   }
   h2 {
     margin:auto;
     color :rgba(0,0,0,0.7);
   }
   a {
     text-decoration:none;
     color:#562728;
     padding:5px;
     
   }
  </style>
 
</head>
<body>
  
  <!--<div class="container">
     <div class="header">
       <h2>WELCOME TO LEZY FRAMEWORK</h2>
       <span>simple framework php</span>
       <hr>
       <p>Default Controller app/controller/Welcome.php</p>
      
       <hr>
       
       <div>
         <a href="">Page</a>
         <a href="">Github</a>
         
         <a href="">Guide</a>
         
       </div>
     </div>
  </div>-->
  
  
  <form action="<?= baseUrl("welcome/index"); ?>" method="post">
    <ul>
    <li>
      <input type="text" value="<?= $this->input->setValue("name")?>" name="name">
      <?= $this->form_valid->valid_message("name","<span style='color:#800000'>","</span>"); ?>
    </li>
    
    <li>
      <input type="text" value="<?= $this->input->setValue("email");?>" name="email">
      <?= $this->form_valid->valid_message("email","<span style='color:#800000'>","</span>"); ?>
    </li>
  
    <button type="submit">tombol</button>
    </ul>
  </form>
  
</body>
</html>